﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vinger
{
    class Program
    {
        static void Main(string[] args)
        {

            string input = "zmienna";
            string key = KeyGenerator.RandomString(input.Length);
            string output = String.Empty;
            Dictionary<char, int> alphabet;
            Stopwatch timer = new Stopwatch();
            timer.Start();
            key = key.ToLower();
            if (key.Length >= input.Length)
            {
                alphabet = new Dictionary<char, int>();
                char c = 'a';
                alphabet.Add(c, 0);

                for (int i = 1; i < 26; i++)
                {
                    alphabet.Add(++c, i);
                }
                output = Encryptor.Crypt(input, key, alphabet);
            }
            timer.Stop();

            Console.Write(output);
            Console.Write(Environment.NewLine + "Encrypted in: " + timer.ElapsedMilliseconds + " ms.");
            Console.Read();
        }
    }
}
